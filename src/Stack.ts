export class Stack<STACK> {
  // Khởi tạo bộ nhớ ngăn xếp
  private items: STACK[] = [];

  //STACK[] Giá trị khởi tạo ban đầu
  constructor(initialData: Array<STACK> = []) {
    this.items.push(...initialData)
  }

  // Xóa tất cả các item trong stack
  clear(): void {
    this.items.splice(0, this.items.length);
  }
 
  // Tìm item có trong stack, có trả về true
  contains(item: STACK): boolean {
    return this.items.indexOf(item) >= 0;
  }

  // Thêm item vào đỉnh stack
  push(item: STACK): void {
    this.items.push(item);
  }

  // Xóa item phía đỉnh ngăn xếp
  pop(): STACK {
    return this.items.pop();
  }

  // Xem phần tử đầu stack mà không xóa nó
  peek(): STACK {
    if (this.isEmpty())
      return undefined;
    else
      return this.items[this.items.length - 1];

  }

  // Kiểm tra stack rỗng không, rỗng trả về true
  isEmpty(): boolean {
    return this.items.length === 0;
  }

  // Kích thước stack
  size(): number {
    return this.items.length;
  }
}