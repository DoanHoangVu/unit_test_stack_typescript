import { expect } from "chai";
import { Stack } from "../src";

describe("Stack", () => {
  it("can be initialized without item", () => {
    const s = new Stack<string>();
    expect(s.size()).to.equal(0);
  });

  it("can be initialized with items", () => {
    const s = new Stack<string>(["Mon", "Tue", "Web", "Thu"]);
    expect(s.size()).to.equal(4);
  });

  it("can be push", () => {
    const s = new Stack<string>(["Mon", "Tue", "Web", "Thu"]);
    s.push("Fri");
    expect(s.size()).to.equal(5);
    expect(s.peek()).to.equal("Fri");
  });

  it("can be pop", () => {
    const s = new Stack<string>(["Mon", "Tue", "Web", "Thu"]);
    expect(s.pop()).to.equal("Thu");
    expect(s.size()).to.equal(3);
  });

  it("can be peek", () => {
    const s = new Stack<string>(["Mon", "Tue", "Web", "Thu"]);
    expect(s.peek()).to.equal("Thu");
    expect(s.size()).to.equal(4);
  });

  it("returns true when stack empty", () => {
    const s = new Stack<string>();
    expect(s.isEmpty()).to.be.true;
  });

  it("return false when stack not empty", () => {
    const s = new Stack<string>(["Mon", "Tue", "Web", "Thu"]);
    expect(s.isEmpty()).to.be.false;
  });

  it("can not pop when no elements", () => {
    const s = new Stack<string>();
    expect(s.pop()).to.be.undefined;
  });

  it("can not peek when empty", () => {
    const s = new Stack<string>();
    expect(s.peek()).to.be.undefined;
  });

  it("can be clear", () => {
    const s = new Stack<string>(["Mon", "Tue", "Web", "Thu"]);
    expect(s.clear());
    expect(s.isEmpty()).to.be.true;
  });

  it("return true when stack contain element", () => {
    const s = new Stack<string>(["Mon", "Tue", "Web", "Thu"]);
    expect(s.contains("Tue")).to.be.true;
  });

  it("return false when stack not contain element", () => {
    const s = new Stack<string>(["Mon", "Tue", "Web", "Thu"]);
    expect(s.contains("Sun")).to.be.false;
  });
});